# aesthetic programming translation 繁體中文

hi, this is the work log of translating "aesthetic programming" http://www.openhumanitiespress.org/books/titles/aesthetic-programming/ in 繁體中文 traditional chinese.

**July** skim through aesthetic programming and reading closely from beginning to chapter 2 <p>
**Aug 2** Arrange workflow with winnie. begining with chapter 3 translation <p>
**Aug 3** throw everything into google translate and see what it suggest <p>


| eng | google suggestion | https://p5js.org/zh-Hans/ check | my suggestion | decision |
| ------ | ------ | ------ | ------ | ------ |
| Aesthetic Programming | 美學程序設計 | | 感知編程、美學程式設計 | |
| Preface | 前言 | | | |
| Design notes | 設計筆記 | | | |
| 1. Getting started | 1. 入門 | | | |
| 2. Variable geometry | 2. 可變幾何 | | 變數幾何 | |
| 3. Infinite loops | 3. 無限循環 | | | |
| 4. Data capture | 4. 數據採集 | | | |
| 5. Auto-generator | 5. 自動發電機 | | 自動生成 | |
| 6. Object abstraction | 6. 對象抽象 | | 抽象物件 | |
| 7. Vocable code | 7. 語音代碼 | | 聲音程式 | |
| 8. Que(e)ry data | 8. 查詢數據 | | | |
| 9. Algorithmic procedures | 9. 算法程序 | | 演算程序 | |
| 10. Machine unlearning | 10.機器學習 | | | |
| Afterword: Recurrent imaginaries | 後記：反復出現的想像 | | | |
| Bibliography | 參考書目 | | | |
| List of Projects | 項目清單 | | | |
| Showcase | 展示櫃 | | | |
| Acknowledgments | 致謝 | | | |


**Aug 6** register GitLab, attempt to fork the original chapter 3 repository https://gitlab.com/aesthetic-programming/book/-/tree/master/source/1-GettingStarted <p>

**Aug 23** finished chapter 3 translation, uploaded on HackMD https://hackmd.io/Mypw1oeCTseK3rTUB0zhHA?view
**Aug 24** translation revisions and suggestions made by winnie and andrew
key issues:  
* loop is translated as 迴圈 instead of 循環 
* translation() remain as 平移
* syntax related phrase emphasis `added`
* successfully forked Aesthetic Programming gitLab repositiory
* try out what is the problem with ATOM editor on my laptop

**Sep 2**
arrange a meeting to discuss

> Does 2 Sep at 1600 TW (09.00 UK/10.00 DK) time work for you? We're thinking to discuss a few things:
specific issues with the chapter and find some the common handling for other chapters.
Potential publishers and timeline.
grant application and details (information regarding for next application) .
Workflows and collaboration in TW context.
Any other things ...

**Sep 20**
work log move to Etherpad
https://ctp.cc.au.dk/pad/p/Aesthetic_Programming_-_TW

* **9/22** 
* Start revising google translation from beginning, google doc crush (might due to irregular format like class table and codes)
* Thinking about how to translate book title and chapters: aesthetic programming 美感程式設計？感知程式設計？美感編程？ machine unlearning 機器反學習 https://www.cw.com.tw/article/5094061
* **9/23**
*  create iCloud link, collaborate here to comment and make changes (seems to only allow mac devices):  https://www.icloud.com/pages/0jAow1iIEHZWpDqndXrJBFMKg#Aesthetic_Programming
*  finished revising cover page and table of content (page 1-5)
* **9/24**
*  Try to migrate the revised contents, as well as, those that yet need to be revised from iCloud page to HackMD: https://hackmd.io/@hT50R4BuR1aenJgRJ63BJw/rkmLaK9Xt
*  HackMD + CSS 直書 how to make chinese horizontal book layout reference: https://hackmd.io/@hackmd/hackmd-new-blog
* revision process 9/23, 24 combined 4000 words (from cover page to first two sections on preface)
* **9/25** Round 1: 10 am to 5pm revision process 5000 words (from preface/beginning to open publishing). 
* Round 2: 7:30~11:30PM formating from preface to 1. Getting started
* **9/26** 2-4pm second time revision and highlight from preface/beginning to open publishing (one hour),  first time revision of preface/flow of context (three paragraph) 
* **9/28** 2:30-6:00 pm finish revising flow of content section
* **9/30** 9-11PM revise from The book object to Note. 8 <p>

**10/2** 2-2:30 Note.8-14
* **10/2** 8:40 revise from Note.15 
* **10/28** preface/ note. 14 -23 翻譯統整為 Readme 解讀、Runme 編寫 <p>

**11/1** Go through Preface again
* **11/2** formating for Vocable code (half-way through)
* **11/4** formating from beginning to end (add images and markdown language)
* **11/18** contact Social Publishing to check if they are able to participate in the 11/25 meeting.
* **11/22** go through Design notes
* begin Chapter 1 Getting Started
* **11/29**: line 1-9, 266-272
* **11/30**:line 10-25, 272-298 
* **12/1**: line 25-163, 298-332<p>

**12/2** funding for translation work? seeking for collective evaluation and subsequent budgeting for funding application. reference: work peirod August - November (4 month), completed translation section: preface + 3. Infinity Loop, word count around 20,000 ~ 44,000. 
